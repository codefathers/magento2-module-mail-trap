<?php

namespace Cf\MailTrap\Plugin\Mail\Template;


use Magento\Framework\Mail\Exception\InvalidArgumentException;
use Magento\Framework\Mail\TransportInterface;
use Magento\Framework\Mail\Template\TransportBuilder as Subject;


/**
 * Fact one: The Mail-Transport and its Message-Object doesn't support
 * clearing or resetting once applied receipients.
 * Fact two: To get the store-related config values, we need to wait
 * for the assignment of the template options.
 *
 * So we have to cache all assigned receivers and assign the actual ones
 * just when the transport object will be fetched.
 *
 * Class TransportBuilder
 * @package Cf\MailTrap\Plugin\Mail\Template
 */
class TransportBuilder
{

    /** @var array */
    protected $templateOptions;

    /** @var \Magento\Store\Model\StoreManagerInterface */
    protected $storeManager;

    /** @var \Cf\MailTrap\Model\ConfigFactory */
    protected $configFactory;

    /** @var \Cf\MailTrap\Model\Config */
    protected $config;

    /** @var \Magento\Framework\App\DeploymentConfig */
    protected $deploymentConfig;

    /** @var array */
    protected $addTo;

    /** @var array */
    protected $addCc;

    /** @var array */
    protected $addBcc;

    /** bool */
    protected $applying = false;


    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\DeploymentConfig $deploymentConfig,
        \Cf\MailTrap\Model\ConfigFactory $configFactory
    )
    {
        $this->storeManager = $storeManager;
        $this->configFactory = $configFactory;
        $this->deploymentConfig = $deploymentConfig;
        $this->clear();
    }


    /**
     *
     */
    protected function clear()
    {
        $this->templateOptions = [];
        $this->config = null;
        $this->addTo = [];
        $this->addCc = [];
        $this->addBcc = [];
        $this->applying = false;
    }

    /**
     * @param Subject $subject
     * @param $options
     */
    public function beforeSetTemplateOptions(Subject $subject, $options)
    {
        $this->templateOptions = $options;
    }

    /**
     * @param Subject $subject
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\ValidatorException
     */
    public function beforeGetTransport(Subject $subject)
    {
        $config = $this->getConfig();
        $config->validate();
        if ($config->hasRedirects()) {
            $this->applyRedirects($subject, $config->getRedirects());
        } else {
            $this->applyDefaults($subject);
        }
    }

    /**
     * @param Subject $subject
     * @param $transport
     * @return mixed
     */
    public function afterGetTransport(Subject $subject, $transport)
    {
        $this->clear();
        return $transport;
    }

    /**
     * @param Subject $subject
     * @param $proceed
     * @param $address
     * @param string $name
     * @return Subject
     */
    public function aroundAddTo(Subject $subject, $proceed, $address, $name = '')
    {
        if ($this->applying) {
            return $proceed($address, $name);
        }
        $this->addTo[] = [$address, $name];
        return $subject;
    }

    /**
     * @param Subject $subject
     * @param $proceed
     * @param $address
     * @param string $name
     * @return Subject
     */
    public function aroundAddCc(Subject $subject, $proceed, $address, $name = '')
    {
        if ($this->applying) {
            return $proceed($address, $name);
        }
        $this->addCc[] = [$address, $name];
        return $subject;
    }

    /**
     * @param Subject $subject
     * @param $proceed
     * @param $address
     * @return Subject
     */
    public function aroundAddBcc(Subject $subject, $proceed, $address)
    {
        if ($this->applying) {
            return $proceed($address);
        }
        $this->addBcc[] = $address;
        return $subject;
    }

    /**
     * @param Subject $subject
     */
    protected function applyDefaults(Subject $subject)
    {
        $this->applying = true;
        foreach ($this->addTo as $item) {
            $subject->addTo($item[0], $item[1]);
        }
        foreach ($this->addCc as $item) {
            $subject->addCc($item[0], $item[1]);
        }
        foreach ($this->addBcc as $item) {
            $subject->addBcc($item);
        }
        $this->applying = false;
    }

    /**
     * @param Subject $subject
     * @param $items
     */
    protected function applyRedirects(Subject $subject, $items)
    {
        $this->applying = true;
        $main = array_shift($items);
        $subject->addTo($main);
        foreach ($items as $email) {
            $subject->addCc($email);
        }
        $this->applying = false;
    }

    /**
     * @return \Cf\MailTrap\Model\Config
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getConfig(): \Cf\MailTrap\Model\Config
    {
        if (!$this->config) {
            $id = (isset($this->templateOptions['store'])) ? (int)$this->templateOptions['store'] : 0;
            $store = $this->storeManager->getStore($id);
            $this->config = $this->configFactory->get($store);
        }
        return $this->config;
    }

}
