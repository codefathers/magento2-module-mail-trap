<?php

namespace Cf\MailTrap\Model;

use Magento\Framework\Exception\ValidatorException;
use Magento\Store\Api\Data\StoreInterface as Store;
use Magento\Store\Model\ScopeInterface;


/**
 * Class Config
 * @package Cf\MailTrap\Model
 */
class Config
{

    /** @var string */
    const PATH = 'system/smtp';

    /** @var array */
    protected $redirectTo;

    /** @var \Magento\Store\Api\Data\StoreInterface */
    protected $store;

    /** @var \Magento\Framework\App\Config\ScopeConfigInterface */
    protected $scopeConfig;

    /**
     * Config constructor.
     * @param Context $context
     * @param StoreManager $storeManager
     */
    public function __construct(
        \Magento\Store\Api\Data\StoreInterface $store,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        $this->store = $store;
        $this->scopeConfig = $scopeConfig;
    }


    /**
     * @return string
     */
    protected function getValue($var)
    {
        $path = self::PATH . '/' . $var;
        $result = $this->scopeConfig->getValue($path, ScopeInterface::SCOPE_STORE, $this->store);
        return $result;
    }

    /**
     * @return string
     */
    protected function getString($key)
    {
        return (string)$this->getValue($key);
    }

    /**
     * @return int
     */
    protected function getInt($key)
    {
        return (int)$this->getValue($key);
    }

    /**
     * @return bool
     */
    protected function getBool($key)
    {
        $state = trim(strtolower((string)$this->getValue($key)));
        if (in_array($state, ['1', 'yes', 'enabled', 'true', 'ok', 'yep'])) {
            return true;
        }
        if (in_array($state, ['0', 'no', 'disabled', 'false', 'none'])) {
            return false;
        }
        return (bool)$state;
    }

    /**
     * @throws ValidatorException
     */
    public function validate()
    {
        $redirects = $this->getRedirects();
        if ($this->hasRedirects() && empty($redirects)) {
            $str = trim((string)$this->getString('redirect_to'));
            throw new ValidatorException(__('Please check redirection configuration. There is something wrong with the entered email: %1', $str));

        }
    }


    /**
     * @return bool
     */
    public function hasRedirects(): bool
    {
        $str = $this->getRedirectTo();
        return (strlen($str)) ? true : false;
    }

    /**
     * @return array
     */
    public function getRedirects(): array
    {
        if (!isset($this->redirects)) {
            $this->redirects = [];
            $value = $this->getRedirectTo();
            if ($value) {
                $value = str_replace([';', ' '], ',', $value);
                $items = explode(',', $value);
                foreach ($items as $mail) {
                    $mail = trim($mail);
                    if ($mail && filter_var($mail, FILTER_VALIDATE_EMAIL)) {
                        $this->redirects[] = $mail;
                    }
                }
            }
        }
        return $this->redirects;



        return $this->getRedirectTo();
    }

    /**
     * @return string
     */
    public function getRedirectTo(): string
    {
        return trim((string)$this->getString('redirect_to'));
    }


}
