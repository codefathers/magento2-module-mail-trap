<?php

namespace Cf\MailTrap\Model;

use Magento\Store\Api\Data\StoreInterface as Store;

/**
 * Class Config
 */
class ConfigFactory
{


    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;


    /** @var string */
    protected $subject;


    /** @var \Magento\Framework\App\Config\ScopeConfigInterface */
    protected $scopeConfig;


    /** @var \Magento\Store\Model\StoreManagerInterface */
    protected $storeManager;


    /** @var array */
    protected $instances = array();


    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        $subject = Config::class
    )
    {
        $this->objectManager = $objectManager;
        $this->subject = $subject;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
    }


    /**
     * @param Store|null $store
     * @return \Cf\MailTrap\Model\Config
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function create(Store $store = null)
    {
        $store = ($store) ? $store : $this->storeManager->getStore();
        return $this->objectManager->create($this->subject, ['store' => $store]);
    }


    /**
     * @param Store|null $store
     * @return \Cf\MailTrap\Model\Config
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get(Store $store = null)
    {
        $store = ($store) ? $store : $this->storeManager->getStore();
        if (!isset($this->instances[$store->getId()])) {
            $this->instances[$store->getId()] = $this->create($store);
        }
        return $this->instances[$store->getId()];
    }

}
