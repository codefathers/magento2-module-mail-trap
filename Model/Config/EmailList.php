<?php

namespace Cf\MailTrap\Model\Config;


/**
 * Class EmailList
 * @package Cf\MailTrap\Model\Config
 */
class EmailList extends \Magento\Framework\App\Config\Value
{


    /**
     * @return EmailList
     */
    public function beforeSave()
    {
        $this->setData('value', $this->formatValue($this->getData('value')));
        return parent::beforeSave();
    }


    /**
     * @param $value
     * @return string|string[]|null
     */
    protected function formatValue($value)
    {
        $value = str_replace([' ', ';'], ',', trim($value));
        $value = preg_replace('/\,+/i', ', ', $value);
        return $value;
    }

}
