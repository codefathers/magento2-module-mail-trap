require([
        'jquery',
        'mage/translate',
        'jquery/validate'],
    function ($) {
        $.validator.addMethod(
            'validate-email-list', function (v) {
                var valid = 0;
                var fails = 0;
                v = v.trim();
                if (!v.length) {
                    return true;
                }
                var parts = v.split(',');
                for (var n = 0; n < parts.length; n++) {
                    var email = parts[n].trim();
                    if (!email.length) {
                        continue;
                    }
                    var validRegExp = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
                    if (email.search(validRegExp) === -1) {
                        fails++;
                    } else  {
                        valid++;
                    }
                }
                return (!fails && valid == parts.length);
            }, $.mage.__('Invalid email - check your entry. Note: Multiple emails must be comma separated'));
    }
);


console.log('adasasd');
